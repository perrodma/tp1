#include "main.h"

void lancer_allegro(){

    // Lancer allegro et le mode graphique
    allegro_init();
    install_keyboard();
    install_mouse();

    set_color_depth(desktop_color_depth());

    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED,800,600,0,0)!=0)
    {
        allegro_message("prb gfx mode");
        allegro_exit();
        exit(EXIT_FAILURE);
    }

}

BITMAP* imgload(char* name){

// Chargement de l'image (l'allocation a lieu en même temps)
    BITMAP* image  =  load_bitmap(name,NULL);
    // Vérification que l'image est bien chargée (dans le cas contraire image vaut NULL)
    // TOUJOURS LE FAIRE CAR ON N'EST JAMAIS CERTAIN DE BIEN TROUVER L'IMAGE
    if (!image)
    {
        allegro_message("pas pu trouver %s",name);
        allegro_exit();
        exit(EXIT_FAILURE);
    }
    return image ;

}

t_sprite* charger_images(){

t_sprite* prov = malloc(sizeof(t_sprite));
prov->matrice[0] = imgload("images/img0.bmp");
prov->matrice[1] = imgload("images/img1.bmp");
prov->matrice[2] = imgload("images/img2.bmp");
prov->matrice[3] = imgload("images/img3.bmp");
prov->matrice[4] = imgload("images/img4.bmp");
prov->matrice[5] = imgload("images/img2.bmp");
prov->matrice[6] = imgload("images/img6.bmp");
prov->matrice[7] = imgload("images/img7.bmp");
prov->matrice[8] = imgload("images/img8.bmp");
prov->perso = imgload("images/perso.bmp");
prov->fin = imgload("images/fin.bmp");
prov->objet[0] = imgload("images/pistolet.bmp");
prov->objet[1] = imgload("images/cube.bmp");
prov->objet[2] = imgload("images/legerte.bmp");
prov->mechant = imgload("images/mechant.bmp");

return prov ;
}

void afficher_matrice(t_sprite* image, t_matrice* carte,BITMAP* page ){
int i=0,j = 0 ;

for ( i = 0 ; i< SCREEN_H/CASE ; i ++ ){
    for ( j = 0 ; j < SCREEN_W/CASE ; j ++ ){

         blit(image->matrice[carte->matrice[i][j]],page,0,0,j*CASE,i*CASE, image->matrice[carte->matrice[i][j]]->w, image->matrice[carte->matrice[i][j]]->h);

    }
}
}

void afficher_perso(t_sprite* image, t_perso* perso,BITMAP* page,int ident , int level){
static int x=0,y=0 ; static int level2 ; static int compteur;
if(compteur == 0 )maj_coord(ident,perso->pos_x,perso->pos_y,&x,&y,level,&level2);
draw_sprite(page, image->perso, CASE*perso->pos_y, CASE*perso->pos_x);
if (x!=0 && y!= 0 && level == level2  )draw_sprite(page, image->perso, CASE*y, CASE*x);
if (compteur == 50) compteur = 0 ;
else compteur ++ ;


}

void afficher_objet(t_sprite* image, t_objet* objet ,BITMAP* page){
int i = 0 ;
for (i = 0 ; i < 100 ; i ++ )
{
    if (objet->bonus[i] != 0 )
    {
    if (objet->bonus[i] == PISTOLET) draw_sprite(page, image->objet[0], CASE*objet->x[i], CASE*objet->y[i]);
    if (objet->bonus[i] == CUBE) draw_sprite(page, image->objet[1], CASE*objet->x[i], CASE*objet->y[i]);
    if (objet->bonus[i] == LEGERTE) draw_sprite(page, image->objet[2], CASE*objet->x[i], CASE*objet->y[i]);


    }

}


}
void afficher_mechant(t_sprite* image,t_ennemis* mechant,BITMAP* page){
int i = 0 ;
for (i = 0 ; i < 100 ; i ++ )
{
    if (mechant->existance[i] )
    {
    draw_sprite(page, image->mechant, CASE*mechant->pos_x[i], CASE*mechant->pos_y[i]);


    }

}


}


void animation_fin(BITMAP* page,t_sprite* image){
    int done = 0 ;
    int x[50]; int y[50];
    int i = 0 ;
    for (i = 0 ; i < 50 ; i ++ ){
    x[i] = 0 ;
    y[i] = rand()%600 ;
    }
    while(!key[KEY_ESC]){
        for (i = 0 ; i < 50 ; i ++ ){

        draw_sprite(page,image->perso,x[i],y[i]);
        if (done<40000 && x[i] < 800)
        {
        x[i] ++ ;
        done ++ ;
        }
        else {
        x[i] -- ;
        y[i] -- ;
        }



        }
        draw_sprite(page,image->fin,0,0);
        blit(page,screen,0,0,0,0,SCREEN_W,SCREEN_H);

        rest(10);
    }



}


