#ifndef TOTOTOTOTO3
#define TOTOTOTOTO3

typedef struct matrice111{

    int** matrice;
    int is_finished;
    int level ;
    int score ;

    }t_matrice;


typedef struct perso111{

    int pos_x ;
    int pos_y ;
}t_perso;

typedef struct objet111{

int bonus[100];
int x[100];
int y[100];
int nbr_pistolet ;
int nbr_cube ;
int nbr_legerte ;

}t_objet ;

typedef struct ennemis111{

int existance[100];
int pos_x[100] ;
int pos_y[100] ;
int direction[100] ;

}t_ennemis;

t_perso* new_perso();
t_matrice* charger_carte(t_perso* perso, int level);
void bouger_perso(t_perso* perso, t_matrice* carte,t_objet* objet);
int test_arriver_defaite(t_perso* perso, t_matrice* carte,t_objet* objet);
t_matrice* changer_level(t_matrice* carte,t_perso* perso, int level);
t_objet* charger_objet(int level);
void gerer_objet(t_matrice* carte,t_objet* objet,t_perso* perso);
int object_allowed(t_objet* objet,t_matrice* carte, t_perso* perso );
t_ennemis* load_ennemi();
int gerer_ennemis(t_ennemis* mechant,t_matrice* carte,t_perso* perso);
void tester_portail(t_perso* perso, t_matrice* carte);
int face_au_mur(t_matrice* carte,t_ennemis* mechant, int i );
t_ennemis* charger_mechant(int level);


#endif // TOTOTOTOTO3

