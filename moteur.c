#include "main.h"

int global_legerte = 0 ;

int direction_up = 0 ;
int direction_down = 0 ;
int direction_right = 0 ;
int direction_left = 0 ;



t_matrice* charger_carte(t_perso* perso,int level1)
{
    t_matrice* prov = malloc(sizeof(t_matrice));
    int i = 0 ; int j = 0 ;
    FILE* fp = NULL ;
    char* level = malloc(sizeof(char)*100);
    sprintf(level,"level/level%d.txt",level1);
    fp =  fopen(level,"r");
    if (!fp) exit(0);

    prov->matrice = malloc(sizeof(int*)*SCREEN_H/CASE);
    for (i = 0 ; i < SCREEN_H/CASE ; i ++ ) prov->matrice[i] = malloc(sizeof(int)*SCREEN_W/CASE );


    for (i=0 ; i < SCREEN_H/CASE ; i ++ )
    {
        for (j = 0 ; j < SCREEN_W/CASE ; j ++ )
        {
            fscanf(fp,"%d",&prov->matrice[i][j]);
            if (prov->matrice[i][j] == 5){
            perso->pos_x = i ;
            perso->pos_y = j ;
            }

        }
    }
    fclose(fp);
    return prov ;


}

t_perso* new_perso()
{
    return malloc(sizeof(t_perso));
}


void bouger_perso(t_perso* perso, t_matrice* carte,t_objet* objet ){




    if (object_allowed(objet,carte,perso)!=2 && key[KEY_UP] && carte->matrice[perso->pos_x-1][perso->pos_y]!=0 && carte->matrice[perso->pos_x-1][perso->pos_y]!=1)
    {
            perso->pos_x -= 1; // mouvement négatif en ordonnées
            direction_up = 1 ;
            direction_down = 0 ;
            direction_right = 0 ;
            direction_left = 0 ;
            if (!global_legerte){
            if ( carte->matrice[perso->pos_x+1][perso->pos_y] != 5&&carte->matrice[perso->pos_x+1][perso->pos_y] != 8)carte->matrice[perso->pos_x+1][perso->pos_y] --;
            else carte->matrice[perso->pos_x+1][perso->pos_y] = 1 ;
            }
            else global_legerte -- ;
    }
    if (object_allowed(objet,carte,perso)!=3 &&key[KEY_DOWN]&& carte->matrice[perso->pos_x+1][perso->pos_y]!=0&& carte->matrice[perso->pos_x+1][perso->pos_y]!=1)
    {
            perso->pos_x += 1; // mouvement positif en ordonnées
            direction_up = 0 ;
            direction_down = 1;
            direction_right = 0 ;
            direction_left = 0 ;
            if (!global_legerte){
            if (carte->matrice[perso->pos_x-1][perso->pos_y] != 5&&carte->matrice[perso->pos_x-1][perso->pos_y] != 8)carte->matrice[perso->pos_x-1][perso->pos_y] -- ;
            else carte->matrice[perso->pos_x-1][perso->pos_y] = 1 ;
            }
            else global_legerte -- ;
    }
    if (object_allowed(objet,carte,perso)!=5 && key[KEY_LEFT]&& carte->matrice[perso->pos_x][perso->pos_y-1]!=0&& carte->matrice[perso->pos_x][perso->pos_y-1]!=1)
    {
            perso->pos_y -= 1; // mouvement négatif en abscisses
            direction_up = 0 ;
            direction_down = 0 ;
            direction_right = 0 ;
            direction_left = 1 ;
            if (!global_legerte){
            if (carte->matrice[perso->pos_x][perso->pos_y+1] != 5&&carte->matrice[perso->pos_x][perso->pos_y+1] != 8)carte->matrice[perso->pos_x][perso->pos_y+1] --;
            else carte->matrice[perso->pos_x][perso->pos_y+1] = 1 ;
            }
            else global_legerte -- ;
    }
    if (object_allowed(objet,carte,perso)!=4 &&key[KEY_RIGHT]&& carte->matrice[perso->pos_x][perso->pos_y+1]!=0 && carte->matrice[perso->pos_x][perso->pos_y+1]!=1)
    {
            perso->pos_y += 1; // mouvement positif en abscisses
            direction_up = 0;
            direction_down = 0 ;
            direction_right = 1 ;
            direction_left = 0 ;
            if (!global_legerte){
            if (carte->matrice[perso->pos_x][perso->pos_y-1] != 5 &&carte->matrice[perso->pos_x][perso->pos_y-1] != 8)carte->matrice[perso->pos_x][perso->pos_y-1] -- ;
            else carte->matrice[perso->pos_x][perso->pos_y-1] = 1 ;
            }
            else global_legerte -- ;
    }


}

int test_arriver_defaite(t_perso* perso, t_matrice* carte,t_objet* objet){
    //D'abord l'arrivé
    int i,j = 0 ;
     for (i=0 ; i < SCREEN_H/CASE ; i ++ )
    {
        for (j = 0 ; j < SCREEN_W/CASE ; j ++ )
        {
            if (carte->matrice[i][j] == 6 ){
                if ( perso->pos_y == j && perso->pos_x == i )
                return ARRIVER ;
            }
        }
    }

    // La défaite

    if (carte->matrice[perso->pos_x+1][perso->pos_y] == 0 || carte->matrice[perso->pos_x+1][perso->pos_y] == 1 || object_allowed(objet,carte,perso) == 3)
    if (carte->matrice[perso->pos_x-1][perso->pos_y] == 0 || carte->matrice[perso->pos_x-1][perso->pos_y] == 1 || object_allowed(objet,carte,perso) == 2)
    if (carte->matrice[perso->pos_x][perso->pos_y+1] == 0 || carte->matrice[perso->pos_x][perso->pos_y+1] == 1|| object_allowed(objet,carte,perso) == 4)
    if (carte->matrice[perso->pos_x][perso->pos_y-1] == 0 || carte->matrice[perso->pos_x][perso->pos_y-1] == 1|| object_allowed(objet,carte,perso) == 5)
    return PERDU ;

    return 0 ;


}

t_matrice* changer_level(t_matrice* carte,t_perso* perso, int level){


    carte = charger_carte(perso,level);
    return carte ;

}

t_objet* new_objet(){

    t_objet* ret = malloc(sizeof(t_objet));
    int i = 0 ;
    for (i=0 ; i < 100 ; i ++ )ret->bonus[i]= 0 ;
    ret->nbr_cube= 0 ;
    ret->nbr_legerte = 0 ;
    ret->nbr_pistolet = 0 ;

return ret;
}

t_objet* charger_objet(int level){

FILE* fp = NULL ;
t_objet* ret = new_objet();
char* niveau = malloc(sizeof(char)*100); int i = 0;
sprintf(niveau,"level/bonus%d.txt",level);
fp = fopen(niveau,"r");
if (!fp) exit(0);
while(!feof(fp)){
    fscanf(fp,"%d %d %d",&ret->bonus[i],&ret->x[i],&ret->y[i]);
    i ++ ;

}
fclose(fp);
return ret;



}

void gerer_objet(t_matrice* carte,t_objet* objet,t_perso* perso){

        int j = 1 ;
        int i = 1 ;


        if (key[KEY_2_PAD] && objet->nbr_pistolet > 0 ){

        objet->nbr_pistolet -- ;

        while ( i != 0 ){
        i = carte->matrice[perso->pos_x +j][perso->pos_y] ;
        if (i != 0 )carte->matrice[perso->pos_x +j][perso->pos_y] = 1 ;

        j ++ ;
        }
        }

        if (key[KEY_8_PAD] && objet->nbr_pistolet > 0 ){

        objet->nbr_pistolet -- ;

        while ( i != 0 ){
        i = carte->matrice[perso->pos_x -j][perso->pos_y] ;
        if (i != 0 )carte->matrice[perso->pos_x -j][perso->pos_y] = 1 ;

        j ++ ;
        }

        }

        if (key[KEY_4_PAD] && objet->nbr_pistolet > 0 ){

        objet->nbr_pistolet -- ;

        while ( i != 0 ){
        i = carte->matrice[perso->pos_x][perso->pos_y-j] ;
        if (i != 0 )carte->matrice[perso->pos_x][perso->pos_y-j] = 1 ;

        j ++ ;
        }

        }

        if (key[KEY_6_PAD] && objet->nbr_pistolet > 0 ){

        objet->nbr_pistolet -- ;

        while ( i != 0 ){
        i = carte->matrice[perso->pos_x][perso->pos_y+j] ;
        if (i != 0 )carte->matrice[perso->pos_x][perso->pos_y+j] = 1 ;

        j ++ ;
        }

        }

        if (key[KEY_9_PAD] && objet->nbr_legerte > 0 ){
        global_legerte ++ ;
        objet->nbr_legerte -- ;

        }

        for (i = 0 ; i < 100 ; i ++ ){
        if (objet->y[i] == perso->pos_x &&objet->x[i] == perso->pos_y ){

        if (objet->bonus[i] == PISTOLET) objet->nbr_pistolet ++ ;
        if (objet->bonus[i] == CUBE) objet->nbr_cube ++ ;
        if (objet->bonus[i] == LEGERTE) objet->nbr_legerte ++ ;
        if (objet->bonus[i] != CUBE) {
        objet->bonus[i] = 0 ;
        objet->x[i] = 0 ;
        objet->y[i] = 0 ;
        }
        else{
        if (direction_down && carte->matrice[objet->y[i]+1][objet->x[i]] != 0 )
        {
            objet->y[i] ++ ;
        }
        if (direction_up && carte->matrice[objet->y[i]-1][objet->x[i]] != 0)
        {
            objet->y[i] -- ;

        }
        if (direction_right && carte->matrice[objet->y[i]][objet->x[i]+1] != 0)
        {
            objet->x[i] ++ ;
        }
        if (direction_left&& carte->matrice[objet->y[i]][objet->x[i]-1] != 0)
        {
        objet->x[i] -- ;
        }



        }

        }

        }



}

int object_allowed(t_objet* objet,t_matrice* carte, t_perso* perso ){
int i = 0 ;
for (i = 0 ; i < 100 ; i ++ )
{
    if (objet->bonus[i] == CUBE){
    if(direction_up && objet->x[i] == perso->pos_y && objet->y[i] ==  perso->pos_x-1 && carte->matrice[objet->y[i]-1][objet->x[i]] == 0 )
    {

        return 2 ;

    }
    if(direction_down && objet->x[i] == perso->pos_y && objet->y[i] ==  perso->pos_x+1  &&carte->matrice[objet->y[i]+1][objet->x[i]] == 0 )
    {

        return 3;

    }
    if(direction_right && objet->x[i] == perso->pos_y+1  && objet->y[i] ==  perso->pos_x && carte->matrice[objet->y[i]][objet->x[i]+1] == 0 )
    {

        return 4 ;

    }
    if(direction_left && objet->x[i] == perso->pos_y-1 && objet->y[i] ==  perso->pos_x  && carte->matrice[objet->y[i]][objet->x[i]-1] == 0 )
    {

        return 5 ;

    }


    }


}
return 1;



}

void tester_portail(t_perso* perso, t_matrice* carte){

int i,j = 0 ;
if (carte->matrice[perso->pos_x][perso->pos_y] == 7){
carte->matrice[perso->pos_x][perso->pos_y] = 1 ;

for (i = 0 ; i < SCREEN_H/CASE ; i ++ ) for (j = 0 ; j < SCREEN_W/CASE ; j ++ ){

    if (carte->matrice[i][j] == 8 ){
    perso->pos_x = i ;
    perso->pos_y = j ;
    }

}

}


}

t_ennemis* load_ennemi(){
t_ennemis* ret = malloc(sizeof(t_ennemis));
int i = 0;
for (i = 0 ; i < 100 ; i ++ ){
ret->direction[i] = 0 ;
ret->existance[i] = 0 ;
ret->pos_x[i] = 0 ;
ret->pos_y[i] = 0 ;

}
return ret ;
}

int gerer_ennemis(t_ennemis* mechant,t_matrice* carte,t_perso* perso){

int i = 0 ; int compteur = 0 ;  int aide =0 ;
int can_right = 1 ; int can_left = 1 ; int can_up = 1 ; int can_down = 1 ;
for (i = 0 ; i < 100 ; i ++ ){

if (mechant->existance[i]){

compteur = 0 ;
    if (!carte->matrice[mechant->pos_y[i]][mechant->pos_x[i]-1]) {compteur ++ ; can_left = 0 ;  }
    if (!carte->matrice[mechant->pos_y[i]][mechant->pos_x[i]+1]) {compteur ++ ; can_right = 0 ; }
    if (!carte->matrice[mechant->pos_y[i]-1][mechant->pos_x[i]]) {compteur ++ ; can_up = 0 ; }
    if (!carte->matrice[mechant->pos_y[i]+1][mechant->pos_x[i]]) {compteur ++ ; can_down = 0 ; }


    if (mechant->direction[i] == LEFT && !can_left){
    aide = rand()%3;
    if(aide ==0 && can_right)mechant->direction[i] = RIGHT ;
    else if(aide == 1 &&can_down)mechant->direction[i] = DOWN ;
    else if(aide == 2 &&can_up)mechant->direction[i] = UP ;


    }
    if (mechant->direction[i] == RIGHT && !can_right){
    aide = rand()%3;
    if(aide == 0 && can_left)mechant->direction[i] = LEFT ;
    else if(aide ==1 &&can_down)mechant->direction[i] = DOWN ;
    else if(aide == 2 &&can_up)mechant->direction[i] = UP ;


    }
    if (mechant->direction[i] == UP && !can_up){
    aide = rand()%3;
    if(aide == 0 && can_right)mechant->direction[i] = RIGHT ;
    else if(aide ==1 &&can_down)mechant->direction[i] = DOWN ;
    else if(aide ==2 &&can_left)mechant->direction[i] = LEFT ;


    }
    if (mechant->direction[i] == DOWN && !can_down){
    aide = rand()%3;
    if(aide == 0 && can_right)mechant->direction[i] = RIGHT ;
    else if(aide == 1 &&can_left)mechant->direction[i] = LEFT ;
    else if(aide == 2 &&can_up)mechant->direction[i] = UP ;


    }


    if(mechant->direction[i] == LEFT && can_left) mechant->pos_x[i] -- ;
    if(mechant->direction[i] == UP&& can_up) mechant->pos_y[i] -- ;
    if (mechant->direction[i] == RIGHT&& can_right) mechant->pos_x[i] ++ ;
    if (mechant->direction[i] == DOWN && can_down) mechant->pos_y[i] ++ ;

    if (carte->matrice[mechant->pos_y[i]][mechant->pos_x[i]] == 1 )mechant->existance[i] = 0 ;
//if (mechant->direction[i] == UP) mechant->pos_y[i] -- ;
//if (mechant->direction[i] == DOWN) mechant->pos_y[i] ++ ;
//if (mechant->direction[i] == RIGHT) mechant->pos_x[i] ++ ;

    if (perso->pos_x == mechant->pos_y[i] && perso->pos_y == mechant->pos_x[i]) return PERDU ;



}
}
return 0 ;

}




t_ennemis* charger_mechant(int level){
t_ennemis* ret= load_ennemi();
char* fichier = malloc(sizeof(char)*100);
sprintf(fichier,"level/mechant%d.txt",level);
FILE * fp = fopen(fichier,("r")); int i = 0 ;
if(!fp) exit(1001);
while (!feof(fp)){

fscanf(fp,"%d %d %d",&ret->pos_x[i], &ret->pos_y[i], &ret->direction[i]);
if (ret->pos_x[i]!=0)ret->existance[i] = 1 ;
i ++;
}



fclose(fp);
return ret;
}



