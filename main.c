#include "main.h"

extern int multijoueur = 0 ;

int main()
{
    srand (time (NULL));
    multijoueur = 0 ;
    lancer_allegro();
    int level = 1;
    BITMAP* page=create_bitmap(SCREEN_W,SCREEN_H);
    int identif = identification();
    clear_bitmap(page);
    t_perso* perso= new_perso();
    t_sprite* image = charger_images();
    t_matrice* carte = charger_carte(perso,level);
    t_objet* objet = charger_objet(level);
    t_ennemis* mechant = charger_mechant(level);

    int terminer = 0 ;
    while(terminer != FIN){
    //afficher_menu(page);

    while(!key[KEY_ESC] && terminer != PERDU){

    afficher_matrice(image,carte,page);
    afficher_objet(image,objet,page);
    afficher_perso(image,perso,page,identif,level);
    afficher_mechant(image,mechant,page);
terminer = test_arriver_defaite(perso,carte,objet);
    if (keypressed()){
    readkey();

terminer = gerer_ennemis(mechant,carte,perso);
    bouger_perso(perso,carte,objet);

    gerer_objet(carte,objet,perso);
    if(key[KEY_R]){objet = charger_objet(level); carte = charger_carte(perso,level); mechant = charger_mechant(level);}
    }

    tester_portail(perso,carte);
    if(terminer == ARRIVER) {level ++ ;objet = charger_objet(level);mechant = charger_mechant(level); carte = charger_carte(perso,level); terminer = 0 ; }

    blit(page,screen,0,0,0,0,SCREEN_W,SCREEN_H);

    rest(10);
    if (terminer == PERDU ){terminer = 0 ;animation_fin(page,image);objet = charger_objet(level); carte = charger_carte(perso,level);  mechant = charger_mechant(level); }
    }

    }

    desidentification(identif);

    return 0;

}END_OF_MAIN()




